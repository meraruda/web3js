import React, { useEffect, useState } from 'react';
import { useWeb3Context } from 'web3-react'
import { Input, Button } from 'antd';
import styled from 'styled-components';
import { Transaction } from 'ethereumjs-tx';
import PK from './pk.json';

const Container = styled.div`
    > div {
        display: flex;
        width: 400px;
        > p {
            flex: 1;
            text-align: right;
        }
    }
`

export default () => {
    const [account, setAccount] = useState('');
    const [balance, setBalance] = useState(0);
    const [node, setNode] = useState();
    const [write, setWrite] = useState();
    const [result, setResult] = useState();
    const context = useWeb3Context();
    const myAccount = '0xbCB43D7a833653DbAfD49c0d091965440D7f83dB';

    useEffect(() => {
        context.setFirstValidConnector(['Infura'])
    }, []);

    useEffect(() => {
        if (context.library) {
            context.library.eth.getNodeInfo((err, res) => {
                setNode(res);
            })
        }
    }, [context.active]);

    const contractAddress = '0x294E52498F1b72d522A38B388a4Ee567f9897BA6';
    const contract = () => (new context.library.eth.Contract([
        {
            "constant": false,
            "inputs": [],
            "name": "read",
            "outputs": [
                {
                    "internalType": "string",
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "internalType": "string",
                    "name": "str",
                    "type": "string"
                }
            ],
            "name": "write",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        }
    ]
        , contractAddress));

    const sendTransaction = () => {
        const data = contract().methods
            .write(write)
            .encodeABI();

        context.library.eth.getTransactionCount(myAccount, (err, txCount) => {
            const txObject = {
                nonce: context.library.utils.toHex(txCount),
                to: contractAddress,
                value: context.library.utils.toHex(context.library.utils.toWei('0', 'ether')),
                gasLimit: context.library.utils.toHex(2100000),
                gasPrice: context.library.utils.toHex(context.library.utils.toWei('6', 'gwei')),
                data: data
            }
            // Sign the transaction
            const tx = new Transaction(txObject, { chain: 'ropsten', hardfork: 'petersburg' });
            tx.sign(Buffer.from(
                PK.key,
                'hex',
            ));

            const serializedTx = tx.serialize();
            const raw = '0x' + serializedTx.toString('hex');

            // Broadcast the transaction
            context.library.eth.sendSignedTransaction(raw, (err, tx) => {
                console.log(err, tx)
            });
        });
    }

    const readFunction = () => {
        contract().methods
            .read()
            .call({
                from: myAccount,
            })
            .then((res) => setResult(res));
    }

    const queryBalance = () => {
        if (context.library) {
            context.library.eth.getBalance(account, (err, val) => {
                setBalance(val);
            });
        }
    }

    return (
        <Container>
            <h4>{context.networkId}</h4>
            <h4>{node}</h4>
            <Input
                style={{ width: '400px' }}
                value={account}
                onChange={(e) => setAccount(e.target.value)}
            />
            <Button
                type="primary"
                onClick={queryBalance}
            >
                Query
            </Button>
            <div>
                <label>Balance:</label>
                <p>
                    {balance}&nbsp;Wei
                </p>
            </div>
            <div>
                <Button
                    type="primary"
                    onClick={readFunction}
                >
                    Read
                </Button>
                {result}
            </div>
            <div>
                <Input
                    value={write}
                    onChange={(e) => setWrite(e.target.value)}

                />
                <Button
                    type="primary"
                    onClick={sendTransaction}
                >
                    Write
                </Button>
            </div>
        </Container>
    );
}