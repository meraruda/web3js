import React from 'react';
import './App.css';
import Web3Provider, { Connectors } from 'web3-react'
import Web3 from 'web3';
import Comp from './Comp';

const { InjectedConnector, NetworkOnlyConnector } = Connectors

const MetaMask = new InjectedConnector({ supportedNetworks: [3] })

const Infura = new NetworkOnlyConnector({
  providerURL: 'https://ropsten.infura.io/v3/927cd37c3e8644f3bfb5decf02eb4d2b'
});

const connectors = { MetaMask, Infura };

function App() {
  return (
    <Web3Provider
      connectors={connectors}
      libraryName="web3.js"
      web3Api={Web3}
    >
      <Comp />
    </Web3Provider>
  );
}

export default App;
